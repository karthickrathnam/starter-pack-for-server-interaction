/**
 * Author: Karthick Rathnam
 */

const AppControl = require('./main_process/AppControl')
const AppSockets = require('./main_process/AppSockets')

//Initiate the http server using express and make it run on app.
//Provide arguments in the order of app name, http address/ip, 
//http port, debug, start option
AppControl.init(
    "Server Monitor", 
    "localhost", 
    4500, 
    false, 
    false
);

//Enable sockets for the created http server.
AppSockets.init(AppControl.httpServer);