/**
 * Initiate and controls the application from the view level. 
 * All the modules, controllers, directives and configurations are handled.
 * 
 * Author: Karthick Rathnam
 */

var App = {
    module: angular.module('ServerMonitor', [
        'ngAnimate',
        'ui.bootstrap',
        'ui.sortable',
        'ui.router',
        'ngTouch',
        'toastr',
        'smart-table',
        "xeditable",
        'ui.slimscroll',
        'ngJsTree',
        'angular-progress-button-styles',
    
        'BlurAdmin.theme',
        'ServerMonitor.pages'
    ]),
  
    init: function(){
        App.user.init();
        App.pages.init();
        
    },

    user: {
        uid: false,

        init: function(){
            //We can also use email or userid or auto-generated passkeys instead of random number.
            App.user.uid = Math.floor(Math.random() * 25456546);

            if(typeof $.cookie("userid") != 'undefined'){
                App.user.uid = $.cookie("userid");
            }

        }
    },

    pages: {
        module: angular.module('ServerMonitor.pages', [
            'ui.router',
    
            'ServerMonitor.pages.dashboard',
            'ServerMonitor.pages.hostMachines'
        ]),

        init: function(){
            App.pages.config();
    
            App.pages.dashboard.init();
            App.pages.hostMachines.init();
    
        },

        config: function(){
            App.pages.module.config(function($urlRouterProvider, baSidebarServiceProvider){
                $urlRouterProvider.otherwise('/dashboard');

            });

        },

        dashboard: {
            module: angular.module('ServerMonitor.pages.dashboard', []),
            socket: false,

            init: function(){
                App.pages.dashboard.config();
                App.pages.dashboard.directives();
                App.pages.dashboard.controllers();

            },

            initSocket: function(callback){
                if(App.pages.dashboard.socket == false){
                    App.pages.dashboard.socket = io('/dashboard-channel');
                }else{
                    callback('success');
                    return;
                }

                App.pages.dashboard.socket.emit('add_user_to_the_channel', {uid: App.user.uid});
                App.pages.dashboard.socket.on('add_user_to_the_channel_reponse', function (responseData) {
                    if(responseData == 'success'){
                        if(typeof $.cookie("userid") == 'undefined'){
                            $.cookie("userid", App.user.uid, { expires: 7 });
                        }
                        
                        callback('success');
                    }else{
                        callback('failed');
                        App.pages.dashboard.destructSocket();
                    }
                });
            },

            destructSocket: function(){
                if(App.pages.dashboard.socket != false){
                    App.pages.dashboard.socket.disconnect();
                    App.pages.dashboard.socket = false;
                }
            },

            config: function(){
                App.pages.dashboard.module.config(function($stateProvider){
                    $stateProvider.state('dashboard', {
                        url: '/dashboard',
                        templateUrl: 'app/pages/dashboard/index.html',
                        title: 'Dashboard',
                        sidebarMeta: {
                            icon: 'ion-android-home',
                            order: 0,
                        },
                    });
                });

                //For chartjs 
                App.pages.dashboard.module.config(function(ChartJsProvider, baConfigProvider){
                    var layoutColors = baConfigProvider.colors;
                    // Configure all charts
                    ChartJsProvider.setOptions({
                        chartColors: [
                            layoutColors.primary, 
                            layoutColors.danger, 
                            layoutColors.warning, 
                            layoutColors.success, 
                            layoutColors.info, 
                            layoutColors.default, 
                            layoutColors.primaryDark, 
                            layoutColors.successDark, 
                            layoutColors.warningLight, 
                            layoutColors.successLight, 
                            layoutColors.primaryLight
                        ],
                        responsive: true,
                        maintainAspectRatio: false,
                        animation: {
                            duration: 0, // general animation time
                        },
                        hover: {
                            animationDuration: 0, // duration of animations when hovering an item
                        },
                        responsiveAnimationDuration: 0, // animation duration after a resize
                        scale: {
                            gridLines: {
                                color: layoutColors.border
                            },
                            scaleLabel: {
                                fontColor: layoutColors.defaultText
                            },
                            ticks: {
                                fontColor: layoutColors.defaultText,
                                showLabelBackdrop: false
                            }
                        },
                        legend: {
                            display: true,
                            position: "bottom",
                            labels: {
                                fontColor: "#333",
                                fontSize: 16
                            }
                        }
                    });
                    // Configure all line charts
                    ChartJsProvider.setOptions('Line', {
                        datasetFill: true,
                        responsive: true,
                        legend: {
                            display: true,
                            position: "bottom",
                            labels: {
                                fontColor: "#333",
                                fontSize: 16
                            }
                        }
                    });
                    // Configure all radar charts
                    ChartJsProvider.setOptions('radar', {
                        scale: {
                            pointLabels: {
                                fontColor: layoutColors.defaultText
                            },
                            ticks: {
                                maxTicksLimit: 5,
                                display: false
                            }
                        }
                    });
                    // Configure all bar charts
                    ChartJsProvider.setOptions('bar', {
                        tooltips: {
                            enabled: false
                        }
                    });

                });

            },

            directives: function(){
                App.pages.dashboard.module.directive('dashboardLivePieChart', function(){
                    return {
                        restrict: 'E',
                        controller: 'DashboardLivePieChartCtrl',
                        templateUrl: 'app/pages/dashboard/livePieChart.html'
                    };
                });

                App.pages.dashboard.module.directive('dashboardHostSelector', function(){
                    return {
                        restrict: 'E',
                        controller: 'DashboardHostSelector',
                        templateUrl: 'app/pages/dashboard/hostSelector.html'
                    };
                });

                App.pages.dashboard.module.directive('selectpicker', function(){
                    return {
                        restrict: 'A',
                        require: '?ngOptions',
                        priority: 1500, // make priority bigger than ngOptions and ngRepeat
                        link: {
                            pre: function(scope, elem, attrs) {
                                elem.append('<option data-hidden="true" disabled value="">' + (attrs.title || 'Select something') + '</option>')
                            },
                            post: function(scope, elem, attrs) {
                                function refresh() {
                                    elem.selectpicker('refresh');
                                }
                        
                                if (attrs.ngModel) {
                                    scope.$watch(attrs.ngModel, refresh);
                                }
                        
                                if (attrs.ngDisabled) {
                                    scope.$watch(attrs.ngDisabled, refresh);
                                }
                        
                                elem.selectpicker({ dropupAuto: false, hideDisabled: true });
                            }
                        }
                    };
                });

                App.pages.dashboard.module.directive('dashboardLineProgressChart', function(){
                    return {
                        restrict: 'E',
                        controller: 'DashboardLineProgressChart',
                        templateUrl: 'app/pages/dashboard/progressLineChart.html'
                    };
                });

                App.pages.dashboard.module.directive('dashboardProcessTable', function(){
                    return {
                        restrict: 'E',
                        controller: 'DashboardProcessTable',
                        templateUrl: 'app/pages/dashboard/displayProcessTable.html'
                    };
                });
                
            },

            controllers: function(){
                var defaultHost = "";
                var defaultHostDetails = "";

                if(typeof $.cookie("defaultSelectedHost") != 'undefined'){
                    defaultHost = JSON.parse($.cookie("defaultSelectedHost"));
                }

                var execSocketProcess = function(){
                    if(defaultHost != ""){
                        var hostId = defaultHost.value;
                    }else{
                        return;
                    }

                    var requestData = {
                        hostId: hostId
                    }

                    App.pages.dashboard.socket.emit('get_host_machine_by_id_request', requestData);
                    App.pages.dashboard.socket.on('get_host_machine_by_id_response', function (responseData) {
                        if(typeof responseData[0] == 'undefined'){
                            return;
                        }

                        defaultHostDetails = responseData[0];
                    });

                };

                App.pages.dashboard.initSocket(function(status){
                    if(status == 'success'){
                        execSocketProcess();
                    }else{
                        console.log("Problem adding user to the socket");
                    }

                });

                App.pages.dashboard.module.controller('DashboardHostSelector', function($scope, $window, $timeout){
                    var execSocketProcess = function(){
                        App.pages.dashboard.socket.emit('get_host_machines_request', '');
                        App.pages.dashboard.socket.on('get_host_machines_response', function (responseData) {
                            //console.log(responseData);
                            if(responseData == 'failed'){
                                return;
                            }
                            
                            var dataSet = new Array();
                            var defaultHostObject = {};
                            $.each(responseData, function( i, val ) {
                                var arrayValues = $.map(val, function(value, index) {
                                    return [value];
                                });
    
                                var dataPushSet = {
                                    label: arrayValues[1] + "(" + arrayValues[2] + " - " + arrayValues[3] + ")",
                                    value: arrayValues[0]
                                };
    
                                dataSet.push(dataPushSet);
                                
                                if(defaultHost != "" && arrayValues[0] == defaultHost.value){
                                    defaultHostObject = dataPushSet;
                                }
                            }); 

                            $scope.hostItemOptions = dataSet;
                            $scope.selectedHostItem = defaultHostObject;
                            $scope.$apply();

                        });
                    };

                    $scope.hostSelectionChanged = function(){
                        $.cookie("defaultSelectedHost", angular.toJson($scope.selectedHostItem), { expires: 7 });
                        $window.location.reload();
                    };

                    App.pages.dashboard.initSocket(function(status){
                        if(status == 'success'){
                            execSocketProcess();
                        }else{
                            console.log("Problem adding user to the socket");
                        }
                    });
    
                    $scope.$on('$destroy', function () {
                        App.pages.dashboard.destructSocket();
                    });
                    
                });

                App.pages.dashboard.module.controller('DashboardLivePieChartCtrl', function($scope, $timeout, baConfig, baUtil){
                    var execSocketProcess = function(){
                        if(defaultHostDetails == ""){
                            return;
                        }

                        var requestData = {
                            hostname: defaultHostDetails.host_name,
                            username: defaultHostDetails.host_username,
                            password: defaultHostDetails.host_password,
                            serverType: defaultHostDetails.host_type,
                            performType: "repeat",
                            refreshRate: 5
                        };

                        App.pages.dashboard.socket.emit('server_process_stream_request', requestData);
                        App.pages.dashboard.socket.on('server_process_stream_response', function (responseData) {
                            console.log(responseData);
                            responseData = JSON.parse(responseData);
                            createPieCharts();
                            updatePieCharts(responseData);
                        });
                    };

                    var pieColor = baUtil.hexToRGB(baConfig.colors.defaultText, 0.2);
                    
                    $scope.charts = [{
                            color: pieColor,
                            description: 'CPU',
                            stats: '',
                            icon: 'person',
                        }, {
                            color: pieColor,
                            description: 'Memory',
                            stats: '',
                            icon: 'money',
                        }, {
                            color: pieColor,
                            description: 'Disk',
                            stats: '',
                            icon: 'refresh',
                        }, {
                            color: pieColor,
                            description: 'Network',
                            stats: '',
                            icon: 'face',
                        }
                    ];
                
                    function createPieCharts(){
                        $('.chart').each(function () {
                            var chart = $(this);
                            chart.easyPieChart({
                            easing: 'easeOutBounce',
                            onStep: function (from, to, percent) {
                                $(this.el).find('.percent').text(Math.round(percent));
                            },
                            barColor: chart.attr('rel'),
                            trackColor: 'rgba(0,0,0,0)',
                            size: 84,
                            scaleLength: 0,
                            animation: 2000,
                            lineWidth: 9,
                            lineCap: 'round',
                            });
                        });
                    }
                    
                    function updatePieCharts(serverData){
                        if(typeof serverData.status == 'undefined' || serverData.status != 'success'){
                            return;
                        }
                    
                        $('.pie-charts .chart').each(function(index, chart) {
                            if(index == 0){
                                $(chart).data('easyPieChart').update(serverData.data.Percentage.CpuUsage);
                            }else if(index == 1){
                                $(chart).data('easyPieChart').update(serverData.data.Percentage.MemoryUsage);
                            }else if(index == 2){
                                $(chart).data('easyPieChart').update(serverData.data.Percentage.DiskUsage);
                            }else if(index == 3){
                                $(chart).data('easyPieChart').update(serverData.data.Percentage.NetworkUsage);
                            }
                        });
                    }

                    App.pages.dashboard.initSocket(function(status){
                        if(status == 'success'){
                            execSocketProcess();
                        }else{
                            console.log("Problem adding user to the socket");
                        }
                    });

                    $scope.$on('$destroy', function () {
                        App.pages.dashboard.destructSocket();
                    });

                });

                App.pages.dashboard.module.controller('DashboardLineProgressChart', function($scope){
                    var execSocketProcess = function(){
                        if(defaultHostDetails == ""){
                            return;
                        }

                        var requestData = {
                            hostname: defaultHostDetails.host_name,
                            username: defaultHostDetails.host_username,
                            password: defaultHostDetails.host_password,
                            serverType: defaultHostDetails.host_type,
                            performType: "repeat",
                            refreshRate: 5
                        };

                        var labels = new Array();
                        var cpuValues = new Array();
                        var memoryValues = new Array();
                        var diskValues = new Array();

                        App.pages.dashboard.socket.emit('server_process_stream_counter_request', requestData);
                        App.pages.dashboard.socket.on('server_process_stream_counter_response', function (responseData) {
                            console.log(responseData);
                            responseData = JSON.parse(responseData);

                            if(typeof responseData.status == 'undefined' || responseData.status != 'success'){
                                return;
                            }

                            $.each(responseData['data']['CpuCounter'], function( i, val ) {
                                $.each(val, function( i, val ) {
                                    if(i == 'Value'){
                                        cpuValues.push(Math.round(val));
                                    }

                                    if(i == 'TimeStamp'){
                                        labels.push(val);
                                    }
                                });
                            });
                            
                            $.each(responseData['data']['MemoryCounter'], function( i, val ) {
                                $.each(val, function( i, val ) {
                                    if(i == 'Value'){
                                        memoryValues.push(Math.round(val));
                                    }
                                });
                            });

                            
                            $.each(responseData['data']['DiskCounter'], function( i, val ) {
                                $.each(val, function( i, val ) {
                                    if(i == 'Value'){
                                        diskValues.push(Math.round(val));
                                    }
                                });
                            });
                        });

                        var passingLabels = new Array();
                        var passingCpuValues = new Array();
                        var passingMemoryValues = new Array();
                        var passingDiskValues = new Array();
                        $scope.series = ['CPU', 'Memory', 'Disk'];

                        setInterval(function(){
                            if(labels.length == 0){
                                return;
                            }

                            var labelsPopped = labels.shift();
                            var cpuValuesPopped = cpuValues.shift();
                            var memoryValuesPopped = memoryValues.shift();
                            var diskValuesPopped = diskValues.shift();

                            if(passingLabels.length > 5){
                                passingLabels.shift();
                                passingCpuValues.shift();
                                passingMemoryValues.shift();
                                passingDiskValues.shift();
                            }

                            passingLabels.push(labelsPopped);
                            passingCpuValues.push(cpuValuesPopped);
                            passingMemoryValues.push(memoryValuesPopped);
                            passingDiskValues.push(diskValuesPopped);       
                            
                            var dataSet = new Array();
                            dataSet.push(passingCpuValues);
                            dataSet.push(passingMemoryValues);
                            dataSet.push(passingDiskValues);
    
                            $scope.labels = passingLabels;
                            $scope.data = dataSet;
  
                            $scope.$apply();
                         }, 3500);
                    };
                    
                    App.pages.dashboard.initSocket(function(status){
                        if(status == 'success'){
                            execSocketProcess();
                        }else{
                            console.log("Problem adding user to the socket");
                        }
                    });

                    $scope.$on('$destroy', function () {
                        App.pages.dashboard.destructSocket();
                    });

                });
                
                App.pages.dashboard.module.controller('DashboardProcessTable', function($scope, toastr, toastrConfig, $compile, $timeout, baConfig, baUtil, $uibModal, baProgressModal){
                    $scope.doKillProcess = function(targetElement){
                        var requestData = {
                            hostname: defaultHostDetails.host_name,
                            username: defaultHostDetails.host_username,
                            password: defaultHostDetails.host_password,
                            serverType: defaultHostDetails.host_type,
                            processId: targetElement.currentTarget.getAttribute("data-id"),
                            performType: "single",
                            refreshRate: 30
                        };

                        App.pages.dashboard.socket.emit('kill_server_process_request', requestData);
                    };
                    
                    var execSocketProcess = function(){
                        if(defaultHostDetails == ""){
                            return;
                        }

                        var requestData = {
                            hostname: defaultHostDetails.host_name,
                            username: defaultHostDetails.host_username,
                            password: defaultHostDetails.host_password,
                            serverType: defaultHostDetails.host_type,
                            performType: "repeat",
                            refreshRate: 30
                        };

                        App.pages.dashboard.socket.emit('get_top_running_process_request', requestData);
                        App.pages.dashboard.socket.on('get_top_running_process_response', function (responseData) {
                            responseData = JSON.parse(responseData);
                            console.log(responseData);
                            //return;
                            if(typeof responseData['status'] == 'undefined' && responseData['status'] == 'failed'){
                                return;
                            }
                            
                            var dataSet = new Array();
                            $.each(responseData['data'], function( i, val ) {
                                var arrayValues = $.map(val, function(value, index) {
                                    return [value];
                                });

                                var actionButtons = "<button data-id='"+ arrayValues[1] +"' ng-click='doKillProcess($event)' class='btn btn-sm btn-danger'>Kill</button>";
                                arrayValues.push(actionButtons);
                        
                                dataSet.push(arrayValues);
                            });
                    
                            processTbl.clear().draw();
                            processTbl.rows.add(dataSet); 
                            processTbl.columns.adjust().draw();
                        });


                        App.pages.dashboard.socket.on('kill_server_process_response', function (responseData) {
                            responseData = JSON.parse(responseData);
                            console.log(responseData);
                            
                            
                            if(responseData['status'] == 'failed'){
                                toastr.error('Problem deleting process', '', {});
                            }else if(responseData['status'] == 'success'){
                                toastr.success('Process deleted successfully', '', {});
                            }else{
                                toastr.error('Problem deleting process', '', {});
                            }
    
                        });
                    };

                    var processTbl = $('#processTbl').DataTable({
                        data: [],
                        columns: [
                            { title: "Process Name" },
                            { title: "Process Id" },
                            { title: "CPU Usage" },
                            { title: "" },
                        ],
                        order: [[ 2, "desc" ]],
                        createdRow: function(row, data, dataIndex) {
                            $compile(angular.element(row).contents())($scope);
                        }
                    });
                    
                    App.pages.dashboard.initSocket(function(status){
                        if(status == 'success'){
                            execSocketProcess();
                        }else{
                            console.log("Problem adding user to the socket");
                        }
                    });

                    $scope.$on('$destroy', function () {
                        App.pages.dashboard.destructSocket();
                    });

                });

            },
            
        },

        hostMachines: {
            module: angular.module('ServerMonitor.pages.hostMachines', []),
            socket: false,
            
            init: function(){
                App.pages.hostMachines.config();
                App.pages.hostMachines.directives();
                App.pages.hostMachines.controllers();

            },

            initSocket: function(callback){
                if(App.pages.hostMachines.socket == false){
                    App.pages.hostMachines.socket = io('/hosts-channel');
                }else{
                    callback('success');
                    return;
                }

                App.pages.hostMachines.socket.emit('add_user_to_the_channel', {uid: App.user.uid});
                App.pages.hostMachines.socket.on('add_user_to_the_channel_reponse', function (responseData) {
                    if(responseData == 'success'){
                        if(typeof $.cookie("userid") == 'undefined'){
                            $.cookie("userid", uid, { expires: 7 });
                        }
                        
                        callback('success');
                    }else{
                        callback('failed');
                        App.pages.hostMachines.destructSocket();
                    }
                });
            },

            destructSocket: function(){
                if(App.pages.hostMachines.socket != false){
                    App.pages.hostMachines.socket.disconnect();
                    App.pages.hostMachines.socket = false;
                }
            },

            config: function(){
                App.pages.hostMachines.module.config(function($stateProvider){
                    $stateProvider.state('hosts', {
                        url: '/hosts',
                        templateUrl: 'app/pages/hosts/index.html',
                        title: 'Host machines',
                        sidebarMeta: {
                            icon: 'ion-android-home',
                            order: 0,
                        },
                    });
                    
                });

            },

            directives: function(){
                App.pages.hostMachines.module.directive('hostmachinesDisplayHosts', function(){
                    return {
                        restrict: 'E',
                        controller: 'HostMachinesDisplayHostsCtrl',
                        templateUrl: 'app/pages/hosts/displayHosts.html'
                    };
                });

            },

            controllers: function(){
                App.pages.hostMachines.module.controller('HostMachinesDisplayHostsCtrl', function($scope, $compile, $timeout, baConfig, baUtil, $uibModal, baProgressModal){
                    var addHostModalCtrl = function($scope, $uibModalStack, modalType){
                        $scope.modalType = modalType;
                        $scope.socketProcessEnabled = false;
                        $scope.errorMsg = "";
                        $scope.errorStatus = 'false';
    
                        $scope.addHostSubmit = function(){
                            $scope.errorMsg = '';
                            $scope.errorStatus = 'false';
    
                            hostName = this.hostName;
                            userName = this.userName;
                            passWord = this.passWord;
                            hostType = this.hostType;
    
                            if(typeof(hostName) === 'undefined' || typeof(userName) === 'undefined' 
                            || typeof(passWord) === 'undefined' || typeof(hostType) === 'undefined'){
                                $scope.errorMsg = "<strong>Error!</strong> You need to fill all the fields.";
                                $scope.errorStatus = 'true';
    
                                return;
                            }
    
                            var requestData = {
                                hostName: hostName,
                                userName: userName,
                                passWord: passWord,
                                hostType: hostType
                            };
    
                            if($scope.socketProcessEnabled){
                                App.pages.hostMachines.socket.emit('add_host_machine_request', requestData);
                                App.pages.hostMachines.socket.on('add_host_machine_response', function (responseData) {
                                    console.log(responseData);
                                    
                                    if(responseData == 'success'){
                                        $uibModalStack.dismissAll();
                                    }else if(responseData == 'invalid-input'){
                                        $scope.errorMsg = "<strong>Error!</strong> Invalid input supplied!";
                                        $scope.errorStatus = 'true';
                                    }else{
                                        $scope.errorMsg = "<strong>Error!</strong> Some problem occured adding hosts.";
                                        $scope.errorStatus = 'true';
                                    }
                                    
                                });
                            }else{
                                $scope.errorMsg = "<strong>Error!</strong> Failed connecting to server. Try to refresh page";
                                $scope.errorStatus = 'true';
                            }
                            
                        };
                        
                        App.pages.hostMachines.initSocket(function(status){
                            if(status == 'success'){
                                $scope.socketProcessEnabled = true;
                            }else{
                                console.log("Problem adding user to the socket");
                            }
                        });    
                    };
                    
                    $scope.doAddHost = function () {
                        $uibModal.open({
                            animation: true,
                            templateUrl: 'app/pages/hosts/hostModals.html',
                            controller : ["$scope", "$uibModalStack", "modalType", addHostModalCtrl],
                            resolve: {
                                modalType: function () {
                                    return 'add';
                                }
                            }
                        });
                    };

                    var editHostModalCtrl = function($scope, $uibModalStack, modalType, injectables){
                        $scope.modalType = modalType;
                        $scope.hostIdEdit = injectables.hostId;
                        $scope.hostNameEdit = injectables.hostName;
                        $scope.userNameEdit = injectables.userName;
                        $scope.hostTypeEdit = injectables.hostType;

                        $scope.socketProcessEnabled = false;
                        $scope.errorMsg = "";
                        $scope.errorStatus = 'false';
    
                        $scope.editHostSubmit = function(){
                            $scope.errorMsg = '';
                            $scope.errorStatus = 'false';
    
                            hostId = this.hostIdEdit;
                            hostName = this.hostNameEdit;
                            userName = this.userNameEdit;
                            passWord = this.passWordEdit;
                            hostType = this.hostTypeEdit;
    
                            if(typeof(hostName) === 'undefined' || typeof(userName) === 'undefined' 
                            || typeof(passWord) === 'undefined' || typeof(hostType) === 'undefined'){
                                $scope.errorMsg = "<strong>Error!</strong> You need to fill all the fields.";
                                $scope.errorStatus = 'true';
    
                                return;
                            }
    
                            var requestData = {
                                hostId: hostId,
                                hostName: hostName,
                                userName: userName,
                                passWord: passWord,
                                hostType: hostType
                            };
    
                            if($scope.socketProcessEnabled){
                                App.pages.hostMachines.socket.emit('edit_host_machine_request', requestData);
                                App.pages.hostMachines.socket.on('edit_host_machine_response', function (responseData) {
                                    console.log(responseData);
                                    
                                    if(responseData == 'success'){
                                        $uibModalStack.dismissAll();
                                    }else if(responseData == 'invalid-input'){
                                        $scope.errorMsg = "<strong>Error!</strong> Invalid input supplied!";
                                        $scope.errorStatus = 'true';
                                    }else{
                                        $scope.errorMsg = "<strong>Error!</strong> Some problem occured adding hosts.";
                                        $scope.errorStatus = 'true';
                                    }
                                    
                                });
                            }else{
                                $scope.errorMsg = "<strong>Error!</strong> Failed connecting to server. Try to refresh page";
                                $scope.errorStatus = 'true';
                            }
                            
                        };
                        
                        App.pages.hostMachines.initSocket(function(status){
                            if(status == 'success'){
                                $scope.socketProcessEnabled = true;
                            }else{
                                console.log("Problem adding user to the socket");
                            }
                        });    
                    };

                    $scope.doEditHost = function(targetElement){
                        var injectables = {
                            hostId: targetElement.currentTarget.getAttribute("data-id"),
                            hostName: targetElement.currentTarget.getAttribute("data-hostname"),
                            userName: targetElement.currentTarget.getAttribute("data-username"),
                            hostType: targetElement.currentTarget.getAttribute("data-hosttype"),
                        };

                        $uibModal.open({
                            animation: true,
                            templateUrl: 'app/pages/hosts/hostModals.html',
                            controller : ["$scope", "$uibModalStack", "modalType", "injectables", editHostModalCtrl],
                            resolve: {
                                modalType: function(){
                                    return 'edit';
                                },
                                injectables: function () {
                                    return injectables;
                                }
                            }
                        });
                    };

                    var deleteHostModalCtrl = function($scope, $uibModalStack, modalType, injectables){
                        $scope.modalType = modalType;
                        $scope.hostIdDelete = injectables.hostId;

                        $scope.socketProcessEnabled = false;
                        $scope.errorMsg = "";
                        $scope.errorStatus = 'false';
    
                        $scope.deleteHostSubmit = function(){
                            $scope.errorMsg = '';
                            $scope.errorStatus = 'false';

                            var requestData = {
                                hostId: $scope.hostIdDelete,
                            };
    
                            if($scope.socketProcessEnabled){
                                App.pages.hostMachines.socket.emit('delete_host_machine_request', requestData);
                                App.pages.hostMachines.socket.on('delete_host_machine_response', function (responseData) {
                                    console.log(responseData);
                                    
                                    if(responseData == 'success'){
                                        $uibModalStack.dismissAll();
                                    }else if(responseData == 'invalid-input'){
                                        $scope.errorMsg = "<strong>Error!</strong> Invalid input supplied!";
                                        $scope.errorStatus = 'true';
                                    }else{
                                        $scope.errorMsg = "<strong>Error!</strong> Some problem occured adding hosts.";
                                        $scope.errorStatus = 'true';
                                    }
                                    
                                });
                            }else{
                                $scope.errorMsg = "<strong>Error!</strong> Failed connecting to server. Try to refresh page";
                                $scope.errorStatus = 'true';
                            }
                            
                        };
                        
                        App.pages.hostMachines.initSocket(function(status){
                            if(status == 'success'){
                                $scope.socketProcessEnabled = true;
                            }else{
                                console.log("Problem adding user to the socket");
                            }
                        });    
                    };

                    $scope.doDeleteHost = function(targetElement){
                        var injectables = {
                            hostId: targetElement.currentTarget.getAttribute("data-id")
                        };

                        $uibModal.open({
                            animation: true,
                            templateUrl: 'app/pages/hosts/hostModals.html',
                            controller : ["$scope", "$uibModalStack", "modalType", "injectables", deleteHostModalCtrl],
                            resolve: {
                                modalType: function(){
                                    return 'delete';
                                },
                                injectables: function () {
                                    return injectables;
                                }
                            }
                        });
                    };

                    var execSocketProcess = function(){
                        App.pages.hostMachines.socket.emit('get_host_machines_request', '');
                        App.pages.hostMachines.socket.on('get_host_machines_response', function (responseData) {
                            console.log(responseData);
                            if(responseData == 'failed'){
                                return;
                            }
                            
                            var dataSet = new Array();
                            $.each(responseData, function( i, val ) {
                                var arrayValues = $.map(val, function(value, index) {
                                    return [value];
                                });

                                var actionButtons = "<button data-id='"+ arrayValues[0] +"' data-hostname='"+ arrayValues[1] +"'  data-username='"+ arrayValues[2] +"' data-hosttype='"+ arrayValues[3] +"' ng-click='doEditHost($event)' class='btn btn-sm btn-info'>Edit</button>&nbsp; <button data-id='"+ arrayValues[0] +"' ng-click='doDeleteHost($event)' class='btn btn-sm btn-danger'>Delete</button>";
                                arrayValues.push(actionButtons);
                        
                                dataSet.push(arrayValues);
                            });
                    
                            hostTbl.clear().draw();
                            hostTbl.rows.add(dataSet); 
                            hostTbl.columns.adjust().draw();
                        });
                    };

                    var hostTbl = $('#hostTbl').DataTable({
                        data: [],
                        columns: [
                            { title: "S.No" },
                            { title: "Hostname" },
                            { title: "Username" },
                            { title: "Type" },
                            { title: "" },
                        ],
                        order: [[ 0, "desc" ]],
                        createdRow: function(row, data, dataIndex) {
                            $compile(angular.element(row).contents())($scope);
                        }
                    });

                    App.pages.hostMachines.initSocket(function(status){
                        if(status == 'success'){
                            execSocketProcess();
                        }else{
                            console.log("Problem adding user to the socket");
                        }
                    });

                    $scope.$on('$destroy', function () {
                        App.pages.hostMachines.destructSocket();
                    });

                });

            },
            
        },

    },
};

(function () {
    'use strict';

    App.init();
})();