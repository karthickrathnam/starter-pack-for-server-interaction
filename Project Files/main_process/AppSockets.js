/**
 * In this page we are handling sockets and system process. 
 * We use socket.io to handle the socket request from the pages.
 * 
 * Author: Karthick Rathnam
 */

const path = require('path');
const AppDatabase = require('./AppDatabase');

var AppSockets = {
    io: null,
    spawn: require("child_process").spawn,
    
    init: function(server){
        AppSockets.io = require('socket.io')(server);

        //Initialize db to perform operation.
        AppDatabase.coreDatabase.init();

        //Initialize sockets.
        AppSockets.pages.dashboard.initSocketChannel();
        AppSockets.pages.hostMachines.initSocketChannel();

    },

    //Handles all the system process we created using node child process.
    systemProcess:{
        processes: Array(),

        //Once spawn is created, we will add that spawn to the record of 
        //corresponding page. If user exit the page, we can indentify and 
        //delete the spawn. 
        addSpawnProcess: function(pageName, userId, spawnProcess){
            if(userId == 'undefined'){
                return;
            }
    
            if(typeof AppSockets.systemProcess.processes[userId] == 'undefined'){
                AppSockets.systemProcess.processes[userId] = Array();
            }
    
            if(typeof AppSockets.systemProcess.processes[userId][pageName] == 'undefined'){
                AppSockets.systemProcess.processes[userId][pageName] = Array();
            }
    
            AppSockets.systemProcess.processes[userId][pageName].push(spawnProcess);
    
        },
    
        //Delete the page's spawn processes when user exit the page. 
        deleteSpawnProcess: function(pageName, userId){
            if(typeof AppSockets.systemProcess.processes[userId] != 'undefined' 
                && typeof AppSockets.systemProcess.processes[userId][pageName] != 'undefined'){

                AppSockets.systemProcess.processes[userId][pageName].forEach(function(spawnProcess) {
                    spawnProcess.kill();
                });
    
                delete AppSockets.systemProcess.processes[userId][pageName];
    
                if (typeof AppSockets.systemProcess.processes[userId] != 'undefined' 
                    && AppSockets.systemProcess.processes[userId].length == 0) {
                    delete AppSockets.systemProcess.processes[userId];
                }
            }

        },

        //Delete all the user's spawn processes if user exit the application.
        deleteAllUsersSpawnProcess: function(){
            console.log("Killing all the spawn process...");
            
            AppSockets.systemProcess.processes.forEach(function(user){
                AppSockets.pages.list.forEach(function(pageName){
                    if(typeof user[pageName] != 'undefined'){
                        user[pageName].forEach(function(spawnProcess) {
                            spawnProcess.kill();
                        });
                    }
                });
            });

            console.log("Deleted all the spawn processes");
            
        },

    },

    pages:{
        list: [
            'dashboard', 
            'hostMachines'
        ],
        
        dashboard: {
            channel: null,
            users: [],

            initSocketChannel: function(){
                AppSockets.pages.dashboard.channel = AppSockets.io.of('/dashboard-channel');
                AppSockets.pages.dashboard.channel.on('connection', function (socket) {
                    AppSockets.pages.dashboard.clientConnected(socket);
                    AppSockets.pages.dashboard.clientDisconnected(socket);

                    AppSockets.pages.dashboard.serverProcessStream(socket);
                    AppSockets.pages.dashboard.serverProcessStreamCounter(socket);
                    AppSockets.pages.dashboard.serverTopRunningProcess(socket);
                    AppSockets.pages.dashboard.serverProcessKill(socket);
                    AppSockets.pages.dashboard.getAllHostMachines(socket);
                    AppSockets.pages.dashboard.getHostMachine(socket);
                    
                });
            },

            clientConnected: function(socket){
                socket.on('add_user_to_the_channel', function(requestData){
                    if(AppSockets.pages.dashboard.users.indexOf(requestData.uid) != -1){
                        //Seems user is already joined.
                        socket.ClientUserId = requestData.uid;
                        socket.emit('add_user_to_the_channel_reponse', 'success');
                        return;
                    }

                    AppSockets.pages.dashboard.users.push(requestData.uid);
                    socket.ClientUserId = requestData.uid;
                    socket.join(requestData.uid);

                    socket.emit('add_user_to_the_channel_reponse', 'success');
                    console.log("Dashboard user joined: "+ socket.ClientUserId);
                });
            },

            clientDisconnected: function(socket){
                socket.on('disconnect', function(){
                    if(typeof socket.ClientUserId == 'undefined'){
                        return;
                    }
                    
                    //Delete all the spawn process created on dashboard page for the user.
                    AppSockets.systemProcess.deleteSpawnProcess('dashboard', socket.ClientUserId);

                    if(AppSockets.pages.dashboard.users.indexOf(socket.ClientUserId) != -1){
                        AppSockets.pages.dashboard.users.splice(AppSockets.pages.dashboard.users.indexOf(socket.ClientUserId), 1);
                    }

                    socket.leave(socket.ClientUserId);
                    console.log("Dashboard user disconnected: "+ socket.ClientUserId);
                    delete socket.ClientUserId;
                });
            },

            serverProcessStream: function(socket){
                socket.on('server_process_stream_request', function(requestData){
                    if(typeof socket.ClientUserId == 'undefined'){
                        return;
                    }

                    var pythonPath = path.join(__dirname, 'server_scripts', 'pyenv', 'Scripts', 'python.exe');
                    var communicatorScript = path.join(__dirname, 'server_scripts', 'communicator.py');
                    var taskToPerform = 'server_process_stream';
    
                    var pythonArgs = [
                        communicatorScript, 
                        taskToPerform, 
                        requestData['hostname'], 
                        requestData['username'], 
                        requestData['password'], 
                        requestData['serverType'],
                        requestData['performType'],
                        requestData['refreshRate']
                    ];

                    var spawnProcess = AppSockets.spawn(pythonPath, pythonArgs);
                    
                    if(typeof spawnProcess != 'undefined'){
                        AppSockets.systemProcess.addSpawnProcess('dashboard', socket.ClientUserId, spawnProcess);
                        
                        spawnProcess.stdout.on('data', function (data){
                            socket.emit('server_process_stream_response', data.toString('utf8'));
                        });
                    }

                });
            },

            serverProcessStreamCounter: function(socket){
                socket.on('server_process_stream_counter_request', function(requestData){
                    if(typeof socket.ClientUserId == 'undefined'){
                        return;
                    }

                    var pythonPath = path.join(__dirname, 'server_scripts', 'pyenv', 'Scripts', 'python.exe');
                    var communicatorScript = path.join(__dirname, 'server_scripts', 'communicator.py');
                    var taskToPerform = 'server_process_stream_counter';
    
                    var pythonArgs = [
                        communicatorScript, 
                        taskToPerform, 
                        requestData['hostname'], 
                        requestData['username'], 
                        requestData['password'], 
                        requestData['serverType'],
                        requestData['performType'],
                        requestData['refreshRate']
                    ];

                    var spawnProcess = AppSockets.spawn(pythonPath, pythonArgs);
                    
                    if(typeof spawnProcess != 'undefined'){
                        AppSockets.systemProcess.addSpawnProcess('dashboard', socket.ClientUserId, spawnProcess);
                        
                        spawnProcess.stdout.on('data', function (data){
                            socket.emit('server_process_stream_counter_response', data.toString('utf8'));
                        });
                    }

                });
            },

            serverTopRunningProcess: function(socket){
                socket.on('get_top_running_process_request', function(requestData){
                    if(typeof socket.ClientUserId == 'undefined'){
                        return;
                    }

                    var pythonPath = path.join(__dirname, 'server_scripts', 'pyenv', 'Scripts', 'python.exe');
                    var communicatorScript = path.join(__dirname, 'server_scripts', 'communicator.py');
                    var taskToPerform = 'server_top_running_process';
    
                    var pythonArgs = [
                        communicatorScript, 
                        taskToPerform, 
                        requestData['hostname'], 
                        requestData['username'], 
                        requestData['password'], 
                        requestData['serverType'],
                        requestData['performType'],
                        requestData['refreshRate']
                    ];

                    var spawnProcess = AppSockets.spawn(pythonPath, pythonArgs);
                    
                    if(typeof spawnProcess != 'undefined'){
                        AppSockets.systemProcess.addSpawnProcess('dashboard', socket.ClientUserId, spawnProcess);
                        
                        spawnProcess.stdout.on('data', function (data){
                            socket.emit('get_top_running_process_response', data.toString('utf8'));
                        });
                    }

                });
            },

            serverProcessKill: function(socket){
                socket.on('kill_server_process_request', function(requestData){
                    if(typeof socket.ClientUserId == 'undefined'){
                        return;
                    }

                    var pythonPath = path.join(__dirname, 'server_scripts', 'pyenv', 'Scripts', 'python.exe');
                    var communicatorScript = path.join(__dirname, 'server_scripts', 'communicator.py');
                    var taskToPerform = 'server_process_kill';

                    var otherParams = {
                        'processId': requestData['processId']
                    };

                    var otherParams = JSON.stringify(otherParams);
                    var otherParamsEncoded = Buffer.from(otherParams).toString('base64');
    
                    var pythonArgs = [
                        communicatorScript, 
                        taskToPerform, 
                        requestData['hostname'], 
                        requestData['username'], 
                        requestData['password'],
                        requestData['serverType'],
                        requestData['performType'],
                        requestData['refreshRate'],
                        otherParamsEncoded
                    ];

                    var spawnProcess = AppSockets.spawn(pythonPath, pythonArgs);
                    
                    if(typeof spawnProcess != 'undefined'){
                        AppSockets.systemProcess.addSpawnProcess('dashboard', socket.ClientUserId, spawnProcess);
                        
                        spawnProcess.stdout.on('data', function (data){
                            socket.emit('kill_server_process_response', data.toString('utf8'));

                            var taskToPerform = 'server_top_running_process';
                            var pythonArgs = [
                                communicatorScript, 
                                taskToPerform, 
                                requestData['hostname'], 
                                requestData['username'], 
                                requestData['password'], 
                                requestData['serverType'],
                                requestData['performType'],
                                requestData['refreshRate']
                            ];
        
                            var spawnProcess = AppSockets.spawn(pythonPath, pythonArgs);
                            
                            if(typeof spawnProcess != 'undefined'){
                                AppSockets.systemProcess.addSpawnProcess('dashboard', socket.ClientUserId, spawnProcess);
                                
                                spawnProcess.stdout.on('data', function (data){
                                    socket.emit('get_top_running_process_response', data.toString('utf8'));
                                });
                            }
                        });
                    }

                });
            },

            getAllHostMachines: function(socket){
                socket.on('get_host_machines_request', function(requestData){
                    if(typeof socket.ClientUserId == 'undefined'){
                        return;
                    }

                    AppDatabase.coreDatabase.getAllHostMachines(function(data){
                        if(data != 'failed'){
                            socket.emit('get_host_machines_response', data);
                        }else{
                            socket.emit('get_host_machines_response', 'failed');
                        }
                    });
                });
            },

            getHostMachine: function(socket){
                socket.on('get_host_machine_by_id_request', function(requestData){
                    if(typeof socket.ClientUserId == 'undefined'){
                        return;
                    }

                    if(typeof (requestData.hostId) == 'undefined'){
                        socket.emit('get_host_machine_by_id_response', 'invalid-input');
                        return;
                    }

                    AppDatabase.coreDatabase.getHostMachine(requestData, function(data){
                        if(data != 'failed'){
                            socket.emit('get_host_machine_by_id_response', data);
                        }else{
                            socket.emit('get_host_machine_by_id_response', 'failed');
                        }
                    });
                });
            },
        },
        
        hostMachines: {
            channel: null,
            users: [],

            initSocketChannel: function(){
                AppSockets.pages.hostMachines.channel = AppSockets.io.of('/hosts-channel');
                AppSockets.pages.hostMachines.channel.on('connection', function (socket) {
                    AppSockets.pages.hostMachines.clientConnected(socket);
                    AppSockets.pages.hostMachines.clientDisconnected(socket);

                    AppSockets.pages.hostMachines.getAllHostMachines(socket);
                    AppSockets.pages.hostMachines.addHostMachine(socket);
                    AppSockets.pages.hostMachines.editHostMachine(socket);
                    AppSockets.pages.hostMachines.deleteHostMachine(socket);
                });
            },

            clientConnected: function(socket){
                socket.on('add_user_to_the_channel', function(requestData){
                    if(AppSockets.pages.hostMachines.users.indexOf(requestData.uid) != -1){
                        //Seems user is already joined.
                        socket.ClientUserId = requestData.uid;
                        socket.emit('add_user_to_the_channel_reponse', 'success');
                        return;
                    }

                    AppSockets.pages.hostMachines.users.push(requestData.uid);
                    socket.ClientUserId = requestData.uid;
                    socket.join(requestData.uid);

                    socket.emit('add_user_to_the_channel_reponse', 'success');
                    console.log("Host machines user joined: "+ socket.ClientUserId);
                });
            },

            clientDisconnected: function(socket){
                socket.on('disconnect', function(){
                    if(typeof socket.ClientUserId == 'undefined'){
                        return;
                    }

                    //Delete all the spawn process created on host machine page for the user.
                    AppSockets.systemProcess.deleteSpawnProcess('hostMachines', socket.ClientUserId);
                    
                    if(AppSockets.pages.hostMachines.users.indexOf(socket.ClientUserId) != -1){
                        AppSockets.pages.hostMachines.users.splice(AppSockets.pages.hostMachines.users.indexOf(socket.ClientUserId), 1);
                    }

                    socket.leave(socket.ClientUserId);
                    console.log("Host machines user disconnected: "+ socket.ClientUserId);
                    delete socket.ClientUserId;
                });
            },

            getAllHostMachines: function(socket){
                socket.on('get_host_machines_request', function(requestData){
                    if(typeof socket.ClientUserId == 'undefined'){
                        return;
                    }
                    
                    AppDatabase.coreDatabase.getAllHostMachines(function(data){
                        if(data != 'failed'){
                            socket.emit('get_host_machines_response', data);
                        }else{
                            socket.emit('get_host_machines_response', 'failed');
                        }
                    });
                });
            },

            deliverHostMachineUpdatedContent: function(socket){
                AppDatabase.coreDatabase.getAllHostMachines(function(data){
                    if(data != 'failed'){
                        //Update the hosts list to the person who initiate the process.
                        socket.emit('get_host_machines_response', data);

                        //Update the hosts list to all the others too except the person who initiate.
                        socket.broadcast.emit('get_host_machines_response', data);
                    }
                });

            },

            addHostMachine: function(socket){
                socket.on('add_host_machine_request', function(requestData){
                    if(typeof socket.ClientUserId == 'undefined'){
                        return;
                    }

                    //Server side validation goes here!
                    if(typeof (requestData.hostName) == 'undefined' ||
                    typeof (requestData.userName) == 'undefined' ||
                    typeof (requestData.passWord) == 'undefined' ||
                    typeof (requestData.hostType) == 'undefined'){
                        socket.emit('add_host_machine_response', 'invalid-input');
                        return;
                    }

                    AppDatabase.coreDatabase.addHostMachine(requestData, function(data){
                        if(data == 'success'){
                            socket.emit('add_host_machine_response', 'success');
                            
                            //Update host tables for connected people.
                            AppSockets.pages.hostMachines.deliverHostMachineUpdatedContent(socket);
                        }else{
                            socket.emit('add_host_machine_response', 'failed');
                        }
                    });
                });
            },

            editHostMachine: function(socket){
                socket.on('edit_host_machine_request', function(requestData){
                    if(typeof socket.ClientUserId == 'undefined'){
                        return;
                    }

                    //Server side validation goes here!
                    if(typeof (requestData.hostName) == 'undefined' ||
                    typeof (requestData.userName) == 'undefined' ||
                    typeof (requestData.passWord) == 'undefined' ||
                    typeof (requestData.hostType) == 'undefined' ||
                    typeof (requestData.hostId) == 'undefined'){
                        socket.emit('edit_host_machine_response', 'invalid-input');
                        return;
                    }
                    
                    AppDatabase.coreDatabase.editHostMachine(requestData, function(data){
                        if(data == 'success'){
                            socket.emit('edit_host_machine_response', 'success');
                            
                            //Update host tables for connected people.
                            AppSockets.pages.hostMachines.deliverHostMachineUpdatedContent(socket);
                        }else{
                            socket.emit('edit_host_machine_response', 'failed');
                        }
                    });
                });
            },

            deleteHostMachine: function(socket){
                socket.on('delete_host_machine_request', function(requestData){
                    if(typeof socket.ClientUserId == 'undefined'){
                        return;
                    }

                    //Server side validation goes here!
                    if(typeof (requestData.hostId) == 'undefined'){
                        socket.emit('delete_host_machine_response', 'invalid-input');
                        return;
                    }

                    AppDatabase.coreDatabase.deleteHostMachine(requestData, function(data){
                        if(data == 'success'){
                            socket.emit('delete_host_machine_response', 'success');
                            
                            //Update host tables for connected people.
                            AppSockets.pages.hostMachines.deliverHostMachineUpdatedContent(socket);
                        }else{
                            socket.emit('delete_host_machine_response', 'failed');
                        }
                    });
                });
            }
        }
    },
    
};

module.exports = AppSockets;