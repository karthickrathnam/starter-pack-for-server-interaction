/**
 * All the database activities carried by this page.
 * Here we use only sqlite3 for the idea of having db with node.
 * 
 * Author: Karthick Rathnam
 */

const path = require('path');
const sqlite3 = require('sqlite3').verbose();

var AppDatabase = {
    init: function(){
        AppDatabase.coreDatabase.init();
        
    },

    coreDatabase: {
        dbConnection: null,

        init: function(){
            //Connecting our sqlite db file.
            var coreDBFile = path.join(__dirname, '../database', 'server_monitor.db');
            AppDatabase.coreDatabase.dbConnection = new sqlite3.Database(coreDBFile, (err) => {
                if(err){
                    AppDatabase.coreDatabase.dbConnection = false;
                }
            });

        },

        getAllHostMachines: function(callback){
            if(AppDatabase.coreDatabase.dbConnection == false){
                callback('failed');
                return;
            }

            var sqlQuery = 'SELECT host_id, host_name, host_username, host_type FROM hosts ORDER BY host_id';
            
            AppDatabase.coreDatabase.dbConnection.all(sqlQuery, function(err, allRows) {
                if(err != null){
                    callback('failed');
                    return;
                }

                callback(allRows);
                return;
            });

        },

        getHostMachine: function(requestData, callback){
            if(AppDatabase.coreDatabase.dbConnection == false){
                callback('failed');
                return;
            }

            var sqlQuery = 'SELECT host_id, host_name, host_username, host_password, host_type FROM hosts WHERE host_id = ? ORDER BY host_id';
            var sqlValues = [
                requestData.hostId
            ];

            AppDatabase.coreDatabase.dbConnection.all(sqlQuery, sqlValues, function(err, allRows) {
                if(err != null){
                    callback('failed');
                    return;
                }

                callback(allRows);
                return;
            });

        },

        addHostMachine: function(requestData, callback){
            if(AppDatabase.coreDatabase.dbConnection == false){
                callback('failed');
                return;
            }

            var sqlQuery = 'INSERT INTO hosts(host_name, host_username, host_password, host_type) VALUES(?, ?, ?, ?)';
            var sqlValues = [
                requestData.hostName, 
                requestData.userName, 
                requestData.passWord, 
                requestData.hostType
            ];

            AppDatabase.coreDatabase.dbConnection.run(sqlQuery, sqlValues, function(err) {
                if(err != null){
                    callback('failed');
                    return;
                }

                callback('success');
                return;
            });

        },

        editHostMachine: function(requestData, callback){
            if(AppDatabase.coreDatabase.dbConnection == false){
                callback('failed');
                return;
            }

            var sqlQuery = 'UPDATE hosts SET host_name = ?, host_username = ?, host_password = ?, host_type = ? WHERE host_id = ?';
            var sqlValues = [
                requestData.hostName, 
                requestData.userName, 
                requestData.passWord, 
                requestData.hostType,
                requestData.hostId
            ];

            AppDatabase.coreDatabase.dbConnection.run(sqlQuery, sqlValues, function(err) {
                if(err != null){
                    callback('failed');
                    return;
                }

                callback('success');
                return;
            });

        },

        deleteHostMachine: function(requestData, callback){
            if(AppDatabase.coreDatabase.dbConnection == false){
                callback('failed');
                return;
            }

            var sqlQuery = 'DELETE FROM hosts WHERE host_id = ?';
            var sqlValues = [
                requestData.hostId
            ];

            AppDatabase.coreDatabase.dbConnection.run(sqlQuery, sqlValues, function(err) {
                if(err != null){
                    callback('failed');
                    return;
                }

                callback('success');
                return;
            });

        }
        
    }
};

module.exports = AppDatabase;