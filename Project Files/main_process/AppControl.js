/**
 * Initiate application and control all the pages.
 * 
 * Author: Karthick Rathnam
 */

const path = require('path');
const glob = require('glob');
const electron = require('electron');
const express = require('express');
const autoUpdater = require('./auto-updater');
const AppSockets = require('./AppSockets');

AppControl = {
    app: electron.app,
    debug: null,
    appName: "",
    httpServer: null,
    expressServer: express(),
    expressAddress: "",
    expressPort: "",
    BrowserWindow: electron.BrowserWindow,
    mainWindow: null,
  

    init: function(appName = "Test app", expressAddress = "localhost", expressPort = "4500", debug = false, startOption = false){
        var alreadyAppOpened = AppControl.makeSingleInstance();
        if (alreadyAppOpened) return AppControl.app.quit();

        if (process.mas) AppControl.app.setName(appName);
        AppControl.appName = appName;
        AppControl.expressAddress = expressAddress;
        AppControl.expressPort = expressPort;
        AppControl.debug = debug;

        // Handle Squirrel on Windows startup events
        switch (startOption) {
            case '--squirrel-install':
              autoUpdater.createShortcut(function () { AppControl.app.quit(); })
              break
            case '--squirrel-uninstall':
              autoUpdater.removeShortcut(function () { AppControl.app.quit(); })
              break
            case '--squirrel-obsolete':
            case '--squirrel-updated':
              AppControl.app.quit();
              break
            default:
              AppControl.startHTTPServer();
              AppControl.initializeApp();
        }
    },

    initializeApp: function(){
        AppControl.app.on('ready', function () {
            AppControl.startMainWindow();
            autoUpdater.initialize();
        });
        
        AppControl.app.on('window-all-closed', function () {
            AppSockets.systemProcess.deleteAllUsersSpawnProcess();

            //If its mac os, then we need to stop the app as it won't by default.
            if (process.platform !== 'darwin') {
              AppControl.app.quit();
            }
        });
        
        AppControl.app.on('activate', function () {
            if (AppControl.mainWindow === null) {
              AppControl.startMainWindow();
            }
        });
    },

    startHTTPServer: function(){
        //Declaring http server using express js, module of node.
        AppControl.expressServer.set('address', AppControl.expressAddress);
        AppControl.expressServer.set('port', AppControl.expressPort);
        AppControl.expressServer.use(express.static(path.join(__dirname, '../src'), { index: 'index.html' }));
        AppControl.expressServer.get('/', function(req, res){
            res.render(path.join(__dirname, '../src', 'index.html'));
        });
        
        
        //Make HTTP server listen to the port and address.
        AppControl.httpServer = AppControl.expressServer.listen(AppControl.expressServer.get('port'), AppControl.expressServer.get('address'), function() {
            var port = AppControl.httpServer.address().port;
            var address = AppControl.httpServer.address().address;
            
            console.log('Server running at http://'+ address + ':' + port);
        });

    },

    startMainWindow: function(){
        const {width, height} = electron.screen.getPrimaryDisplay().workAreaSize;
        var windowOptions = {
            show: false,
            title: AppControl.app.getName()
        };

        if(process.platform === 'linux'){
            windowOptions.icon = path.join(__dirname, '/assets/app-icon/png/512.png');
        }

        AppControl.mainWindow = new AppControl.BrowserWindow(windowOptions);
        //As we are having http server instead of static page, we provide http server url.
        AppControl.mainWindow.loadURL("http://"+ AppControl.expressAddress +":"+ AppControl.expressPort);
        AppControl.mainWindow.maximize();
        AppControl.mainWindow.show();

        // Launch fullscreen with DevTools open, usage: npm run debug
        if(AppControl.debug){
            AppControl.mainWindow.webContents.openDevTools();
            AppControl.mainWindow.maximize();
            require('devtron').install();
        }

        AppControl.mainWindow.on('closed', function () {
            AppControl.mainWindow = null;
        });

    },

    // Make this app a single instance app.
    //
    // The main window will be restored and focused instead of a second window
    // opened when a person attempts to launch a second instance.
    //
    // Returns true if the current version of the app should quit instead of
    // launching.
    makeSingleInstance: function() {
        if (process.mas) return false;

        return AppControl.app.makeSingleInstance(function () {
            if (AppControl.mainWindow) {
                if (AppControl.mainWindow.isMinimized()){
                  AppControl.mainWindow.restore();
                } 

                AppControl.mainWindow.focus();
            }
        });
    },

};

module.exports = AppControl;