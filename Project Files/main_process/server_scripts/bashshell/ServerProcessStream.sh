CPUUsage=`top -bn1 | grep "Cpu(s)" | sed "s/.*, *\([0-9.]*\)%* id.*/\1/" | awk '{print 100 - $1}'`
MemoryUsage=`free | grep Mem | awk '{print $4/$2 * 100.0}'`

echo $CPUUsage
echo $MemoryUsage
exit