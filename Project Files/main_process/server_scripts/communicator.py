#************************************************************************
# Script will communicate with windows/linux server to get the 
# required output from the server
#
# Author: Karthickraja R
#************************************************************************

import sys
import json

taskToPerform = sys.argv[1]
hostName = sys.argv[2]
userName = sys.argv[3]
passWord = sys.argv[4]
serverType = sys.argv[5]
performType = sys.argv[6]
refreshRate = sys.argv[7]

def emitJsonOutput(status = 'failed', data = 'Problem connecting server'):
    returnObject = {
        'status': status,
        'data': data
    }

    print(json.dumps(returnObject))
    sys.stdout.flush()
    return

try:
    otherParams = sys.argv[8]
    otherParams = otherParams.decode('base64')
    otherParams = json.loads(otherParams)
except:
    otherParams = ""

if serverType == 'windows':
    try:
        from WinExec import WinExec

        WinExec(
            hostname = hostName,
            username = userName,
            password = passWord,
            tasktoperform = taskToPerform,
            performtype = performType,
            refreshrate = refreshRate,
            otherparams = otherParams
        )
    except Exception:
        unknownException = str(sys.exc_info()[1])
        emitJsonOutput('failed', unknownException)
        exit()

elif serverType == 'linux':
    try:
        from LinExec import LinExec

        LinExec(
            hostname = hostName,
            username = userName,
            password = passWord,
            tasktoperform = taskToPerform,
            performtype = performType,
            refreshrate = refreshRate,
            otherparams = otherParams
        )
    except Exception:
        unknownException = str(sys.exc_info()[1])
        emitJsonOutput('failed', unknownException)
        exit()