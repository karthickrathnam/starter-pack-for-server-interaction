#************************************************************************
# All the windows execution scripts will be handle here.
#
# Author: Karthickraja R
#************************************************************************

import sys
import os
import time
import base64
import json
import string
from pylib.ConnectNTLM import ConnectNTLM

class WinExec:
    #Peform the NTLM connection to the host machine and make one shell stream for command
    #execution. Then initiate the task where we are requested to perform.
    def __init__(self, hostname, username, password, tasktoperform, performtype, refreshrate, otherparams = ""):
        self.tasktoperform = tasktoperform
        self.performtype = performtype
        self.refreshrate = refreshrate
        self.otherparams = otherparams
        self.hostConnection = False
        self.hostShellStream = False

        credentials = {
            'hostname' : hostname,
            'username' : username,
            'password' : password
        }

        try:
            #Create NTLM connection 
            self.hostConnection = ConnectNTLM(credentials, responsesleeptime = 1, verbose = False)
            if not self.hostConnection.ntlmClientConnection:
                self.emitJsonOutput('failed', 'Problem connecting host machine')
                exit()
                return
                
            #Create shell stream
            self.hostShellStream = self.hostConnection.getShellStream()
            if not self.hostShellStream:
                self.emitJsonOutput('failed', 'Problem creating shell stream on host machine')
                exit()
                return

        except Exception:
            unknownException = str(sys.exc_info()[1])
            self.emitJsonOutput('failed', unknownException)
            exit()

        #If the connection is created without any problem. Lets perform task.
        self.performTask()

    #Terminate the shell stream.
    def exitWinExec(self):
        if not self.hostConnection:
            self.hostConnection.closeShellStream(self.hostShellStream)

    #Emit the json output in valid format.
    def emitJsonOutput(self, status = 'failed', data = 'Problem connecting server'):
        returnObject = {
            'status': status,
            'data': data
        }

        print(json.dumps(returnObject))
        sys.stdout.flush()
        return

    #Function will match the valid task with given flag
    def performTask(self):
        if self.tasktoperform == 'server_process_stream':
            self.serverProcessStream()
        elif self.tasktoperform == 'server_process_stream_counter':
            self.serverProcessStreamCounter()
        elif self.tasktoperform == 'server_top_running_process':
            self.serverTopRunningProcess()
        elif self.tasktoperform == 'server_process_kill':
            self.serverProcessKill()
        else:
            self.emitJsonOutput('failed', 'Flag not matched to perform any of the available tasks.')
            exit()

    #Stream the process percentage from the server.
    #That is, we are getting about to get cpu, memory, network and disk usage percentage.
    def serverProcessStream(self):
        try:
            communicatorFilePath = os.path.dirname(sys.argv[0])       
            communicatorFilePath = os.path.abspath(communicatorFilePath) 

            scriptPathWithName = os.path.join(communicatorFilePath, 'powershell', 'ServerProcessStream.ps1')
            scriptFile = open(scriptPathWithName, 'r') 
            scriptContent = scriptFile.readlines() 
            scriptContent = "".join(scriptContent)
            
            #base64 encode, utf16 little endian. required for windows
            encodedScriptContent = base64.b64encode(scriptContent.encode("utf_16_le"))

            while True:
                stdout, stderr, commandReference = self.hostConnection.executeCommandOnShellStream(
                    self.hostShellStream,
                    "powershell -encodedcommand %s" %(encodedScriptContent.decode('utf-8'))
                )

                #We try 3 maximum attempts to retreive result
                for n in range(0, 3):
                    stdout = stdout.strip()
                    if stdout == "":
                        #Try to read command again
                        time.sleep(2)
                        stdout, stderr = self.hostConnection.readCommentOutputFromShellStream(self.hostShellStream, commandReference)
                    else:
                        break

                stdout = stdout.strip()
                if stdout == "":
                    self.emitJsonOutput('failed', 'We have got an empty string')
                    pass

                try:
                    jsonStdout = json.loads(stdout)
                    if jsonStdout['Status'] != 'success':
                        self.emitJsonOutput('failed', 'Error processing data on server side')
                        pass
                    else:
                        shellOutput = jsonStdout['ShellOutput']
                        self.emitJsonOutput('success', shellOutput)
                        pass
                        
                except Exception:
                    self.emitJsonOutput('failed', 'Invalid output format received')

                if self.performtype == 'repeat':
                    time.sleep(int(self.refreshrate))
                else:
                    break

            self.hostConnection.cleanCommandOnShellStream(self.hostShellStream, commandReference)
            self.exitWinExec()

        except Exception:
            unknownException = str(sys.exc_info()[1])
            self.emitJsonOutput('failed', unknownException)
            exit()
    
    #Stream the process percentage using counter from the server.
    #That is, we are getting about to get cpu, memory, network and disk usage percentage.
    def serverProcessStreamCounter(self):
        try:
            communicatorFilePath = os.path.dirname(sys.argv[0])       
            communicatorFilePath = os.path.abspath(communicatorFilePath) 

            scriptPathWithName = os.path.join(communicatorFilePath, 'powershell', 'ServerProcessStreamCounter.ps1')
            scriptFile = open(scriptPathWithName, 'r') 
            scriptContent = scriptFile.readlines() 
            scriptContent = "".join(scriptContent)
            
            #base64 encode, utf16 little endian. required for windows
            encodedScriptContent = base64.b64encode(scriptContent.encode("utf_16_le"))

            while True:
                stdout, stderr, commandReference = self.hostConnection.executeCommandOnShellStream(
                    self.hostShellStream,
                    "powershell -encodedcommand %s" %(encodedScriptContent.decode('utf-8'))
                )

                #We try 3 maximum attempts to retreive result
                for n in range(0, 3):
                    stdout = stdout.strip()
                    if stdout == "":
                        #Try to read command again
                        time.sleep(2)
                        stdout, stderr = self.hostConnection.readCommentOutputFromShellStream(self.hostShellStream, commandReference)
                    else:
                        break

                stdout = stdout.strip()
                if stdout == "":
                    self.emitJsonOutput('failed', 'We have got an empty string')
                    pass

                try:
                    jsonStdout = json.loads(stdout)
                    if jsonStdout['Status'] != 'success':
                        self.emitJsonOutput('failed', 'Error processing data on server side')
                        pass
                    else:
                        self.emitJsonOutput('success', jsonStdout)
                        pass
                        
                except Exception:
                    unknownException = str(sys.exc_info()[1])
                    self.emitJsonOutput('failed', unknownException)
                    exit()
                    self.emitJsonOutput('failed', 'Invalid output format received')

                if self.performtype == 'repeat':
                    time.sleep(int(self.refreshrate))
                else:
                    break

            self.hostConnection.cleanCommandOnShellStream(self.hostShellStream, commandReference)
            self.exitWinExec()

        except Exception:
            unknownException = str(sys.exc_info()[1])
            self.emitJsonOutput('failed', unknownException)
            exit()

    #Retrieving Top running server process!
    def serverTopRunningProcess(self):
        try:
            communicatorFilePath = os.path.dirname(sys.argv[0])       
            communicatorFilePath = os.path.abspath(communicatorFilePath) 

            scriptPathWithName = os.path.join(communicatorFilePath, 'powershell', 'ServerTopRunningProcess.ps1')
            scriptFile = open(scriptPathWithName, 'r') 
            scriptContent = scriptFile.readlines() 
            scriptContent = "".join(scriptContent)
            
            #base64 encode, utf16 little endian. required for windows
            encodedScriptContent = base64.b64encode(scriptContent.encode("utf_16_le"))

            while True:
                stdout, stderr, commandReference = self.hostConnection.executeCommandOnShellStream(
                    self.hostShellStream,
                    "powershell -encodedcommand %s" %(encodedScriptContent.decode('utf-8'))
                )

                #We try 3 maximum attempts to retreive result
                for n in range(0, 3):
                    stdout = stdout.strip()
                    if stdout == "":
                        #Try to read command again
                        time.sleep(2)
                        stdout, stderr = self.hostConnection.readCommentOutputFromShellStream(self.hostShellStream, commandReference)
                    else:
                        break

                stdout = stdout.strip()
                if stdout == "":
                    self.emitJsonOutput('failed', 'We have got an empty string')
                    pass

                try:
                    jsonStdout = json.loads(stdout)
                    if jsonStdout['Status'] != 'success':
                        self.emitJsonOutput('failed', 'Error processing data on server side')
                        pass
                    else:
                        shellOutput = jsonStdout['ShellOutput']
                        self.emitJsonOutput('success', shellOutput)
                        pass
                        
                except Exception:
                    unknownException = str(sys.exc_info()[1])
                    self.emitJsonOutput('failed', unknownException)
                    exit()
                    self.emitJsonOutput('failed', 'Invalid output format received')

                if self.performtype == 'repeat':
                    time.sleep(int(self.refreshrate))
                else:
                    break

            self.hostConnection.cleanCommandOnShellStream(self.hostShellStream, commandReference)
            self.exitWinExec()

        except Exception:
            unknownException = str(sys.exc_info()[1])
            self.emitJsonOutput('failed', unknownException)
            exit()

    def serverProcessKill(self):
        try:
            communicatorFilePath = os.path.dirname(sys.argv[0])       
            communicatorFilePath = os.path.abspath(communicatorFilePath)

            scriptPathWithName = os.path.join(communicatorFilePath, 'powershell', 'ServerProcessKill.ps1')
            scriptFile = open(scriptPathWithName, 'r') 
            scriptContent = scriptFile.readlines() 
            scriptContent = "".join(scriptContent)

            scriptContent = string.replace(scriptContent, '$IDVAR', self.otherparams['processId'])
            
            #base64 encode, utf16 little endian. required for windows
            encodedScriptContent = base64.b64encode(scriptContent.encode("utf_16_le"))

            while True:
                stdout, stderr, commandReference = self.hostConnection.executeCommandOnShellStream(
                    self.hostShellStream,
                    "powershell -encodedcommand %s" %(encodedScriptContent.decode('utf-8'))
                )

                #We try 3 maximum attempts to retreive result
                for n in range(0, 3):
                    stdout = stdout.strip()
                    if stdout == "":
                        #Try to read command again
                        time.sleep(2)
                        stdout, stderr = self.hostConnection.readCommentOutputFromShellStream(self.hostShellStream, commandReference)
                    else:
                        break

                stdout = stdout.strip()
                if stdout == "":
                    self.emitJsonOutput('failed', 'We have got an empty string')
                    pass

                try:
                    jsonStdout = json.loads(stdout)
                    if jsonStdout['Status'] != 'success':
                        self.emitJsonOutput('failed', 'Error processing data on server side')
                        pass
                    else:
                        shellOutput = jsonStdout['ShellOutput']
                        self.emitJsonOutput('success', shellOutput)
                        pass
                        
                except Exception:
                    unknownException = str(sys.exc_info()[1])
                    self.emitJsonOutput('failed', unknownException)
                    exit()
                    self.emitJsonOutput('failed', 'Invalid output format received')

                if self.performtype == 'repeat':
                    time.sleep(int(self.refreshrate))
                else:
                    break

            self.hostConnection.cleanCommandOnShellStream(self.hostShellStream, commandReference)
            self.exitWinExec()

        except Exception:
            unknownException = str(sys.exc_info()[1])
            self.emitJsonOutput('failed', unknownException)
            exit()