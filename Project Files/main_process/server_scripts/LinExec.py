#************************************************************************
# All the linux execution scripts will be handle here.
#
# Author: Karthickraja R
#************************************************************************

import sys
import os
import time
import base64
import json
from pylib.ConnectSSH import ConnectSSH

class LinExec:
    #Peform the ssh connection to the host machine.
    def __init__(self, hostname, username, password, tasktoperform, performtype, refreshrate, otherparams = ""):
        self.tasktoperform = tasktoperform
        self.performtype = performtype
        self.refreshrate = refreshrate
        self.otherparams = otherparams
        self.hostConnection = False
        self.responsesleeptime = 2
        self.debug = False

        credentials = {
            'hostname' : hostname,
            'username' : username,
            'password' : password
        }

        try:
            #Create connection 
            self.hostConnection = ConnectSSH(credentials, responsesleeptime = self.responsesleeptime, verbose = False)
            if self.hostConnection.sshClientConnection == False:
                self.emitJsonOutput('failed', 'Failed to connect client machine.')
                exit()

        except Exception:
            unknownException = str(sys.exc_info()[1])
            self.emitJsonOutput('failed', unknownException)
            exit()

        #If the connection is created without any problem. Lets perform task.
        self.performTask()

    #Terminate the ssh connection.
    def exitLinExec(self):
        if not self.hostConnection.sshClientConnection:
            self.hostConnection.closeSSHConnection(verbose = debug)

    #Emit the json output in valid format.
    def emitJsonOutput(self, status = 'failed', data = 'Problem connecting server'):
        returnObject = {
            'status': status,
            'data': data
        }

        print(json.dumps(returnObject))
        sys.stdout.flush()
        return

    #Function will match the valid task with given flag
    def performTask(self):
        if self.tasktoperform == 'server_process_stream':
            self.serverProcessStream()
        else:
            self.emitJsonOutput('failed', 'Flag not matched to perform any of the available tasks.')
            exit()

    #Stream the process percentage from the server.
    #That is, we are getting about to get cpu, memory, network and disk usage percentage.
    def serverProcessStream(self):
        try:
            communicatorFilePath = os.path.dirname(sys.argv[0])       
            communicatorFilePath = os.path.abspath(communicatorFilePath) 

            scriptPathWithName = os.path.join(communicatorFilePath, 'bashshell', 'ServerProcessStream.sh')
            scriptFile = open(scriptPathWithName, 'r') 
            scriptContent = scriptFile.readlines() 
            scriptContent = "".join(scriptContent)
            
            while True:
                stdout, stderr = self.hostConnection.executeCommand(scriptContent, verbose = False)
                stdout = stdout.strip()
                if stdout == "":
                    self.emitJsonOutput('failed', 'We have got an empty string')
                    pass

                try:
                    retrievedOuput = stdout.split('\n')
                    Percentage = {
                        'CpuUsage': float(retrievedOuput[0].decode('ascii')),
                        'MemoryUsage': float(retrievedOuput[1].decode('ascii')),
                        'DiskUsage': 0.0,
                        'NetworkUsage': 0.0
                    }

                    overallPacket = {
                        'Status': 'success',
                        'Percentage': Percentage
                    }

                    self.emitJsonOutput('success', overallPacket)
                    pass
                        
                except Exception:
                    self.emitJsonOutput('failed', 'Invalid output format received')

                if self.performtype == 'repeat':
                    time.sleep(int(self.refreshrate))
                else:
                    break
        except Exception:
            unknownException = str(sys.exc_info()[1])
            self.emitJsonOutput('failed', unknownException)
            exit()