# **************************************************************************
#   Library makes easy go for connecting hosts using NTLM with more reliable. 
#
#
#   Although we have pywinrm with us, we are facing hard time to connect
#   to the host when server response get delayed. And also hard to compose
#   the code by finding the options available in pywinrm. So comes this 
#   module, which makes easy go to deal with pywinrm to connect windows hosts. 
#
#   Author: Karthickraja R
# **************************************************************************

import platform
import os
import sys
import subprocess
import time
import csv
import base64
import json
import urllib3
import requests
from winrm.protocol import Protocol
from requests.packages.urllib3.exceptions import InsecureRequestWarning

class ConnectNTLM:
    def __init__(self, credentials, numofattempt = 2, responsesleeptime = 2, verbose = False):
        #As we don't want to include any warnings, we disabled it
        urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
        requests.packages.urllib3.disable_warnings(InsecureRequestWarning)

        self.ntlmClientConnection = False
        self.sleepTimeForServerResponse = responsesleeptime
        self.lastStatusMessage = 'Yet to connect...'
        self.PythonVersion3 = sys.version_info.major >= 3

        if 'hostname' not in credentials or 'username' not in credentials:
            self.updateStatusMessage("hostname or username is not found", verbose)
            return

        self.hostName = credentials['hostname']
        hostName = credentials['hostname']
        userName = credentials['username']
        passWord = credentials['password']

        #Ping test
        serverStatus = self.pingServer(hostName, verbose)
        if not serverStatus:
            return

        self.updateStatusMessage("Trying to connect the host: "+ hostName, verbose)
        counter = 0
        while counter < numofattempt:
            try:
                self.ntlmClientConnection = Protocol(
                    endpoint = 'https://'+ hostName +':5986/wsman',
                    transport = 'ntlm',
                    username = r''+userName,
                    password = passWord,
                    server_cert_validation = 'ignore'
                )

                self.updateStatusMessage("Connected to host: "+ hostName, verbose)
                return
            except Exception:
                unknownException = str(sys.exc_info()[1])
                counter += 1
                self.ntlmClientConnection = False
                self.updateStatusMessage("Error occured: "+ unknownException, verbose)
                return

    def updateStatusMessage(self, message, verbose = False):
        if verbose:
            if platform.system().lower() == 'linux' or platform.system().lower() == 'sunos':
                print("\033[93m VERBOSE: " + message + " \033[0m")
            else:
                print("VERBOSE: " + message)
        self.lastStatusMessage = message
        pass

    def pingServer(self, hostName, verbose = False):
        pingResult = False

        #Try to ping server...
        #We will try 3 attempts to ping server. If it failed on all 3 attempts, then we fix server is offline.
        for n in range(0, 3):
            self.updateStatusMessage("Pinging the host: " + hostName, verbose)
            try:
                output = subprocess.check_output("ping -{} 1 {}".format('n' if platform.system().lower()=="windows" else 'c', hostName), shell=True)
            except Exception:
                unknownException = str(sys.exc_info()[1])
                self.updateStatusMessage(str(n + 1) + " attempt of ping test failed for the host: " + hostName, verbose)
                self.updateStatusMessage("Error occured: "+ unknownException, verbose)
                time.sleep(self.sleepTimeForServerResponse)
                continue
            else:
                #If our ping command executed without facing any error, then its pingable!!!
                pingResult = True
                break

        if pingResult:
            self.updateStatusMessage("Ping test success for the host: "+ hostName, verbose)            
            return True
        else:
            self.updateStatusMessage("Ping test failed for the host: "+ hostName, verbose)            
            return False
        pass

    def getShellStream(self, verbose = False):
        if self.ntlmClientConnection == False:
            return False

        try:
            getShellStream = self.ntlmClientConnection.open_shell()

            #Give 3 attempts to make sure the stream is ready
            for n in range(0, 3):
                if getShellStream:
                    return getShellStream
                else:
                    time.sleep(self.sleepTimeForServerResponse)
        except Exception:
            unknownException = str(sys.exc_info()[1])
            self.updateStatusMessage("Error occured: "+ unknownException, verbose)
            time.sleep(self.sleepTimeForServerResponse)
            pass

        self.updateStatusMessage("No response from the server for create shell stream", verbose)
        return False
    
    def executeCommandOnShellStream(self, shellStream, command, verbose = False):
        stdoutFullString = ''
        stderrFullString = ''
        commandReference = ''

        if self.ntlmClientConnection == False:
            self.updateStatusMessage("Invalid ntlm connection. Can't execute command", verbose)
            return stdoutFullString, stderrFullString

        errorFlag = True
        #Try 2 attempts to execute the command
        for n in range(0, 2):
            self.updateStatusMessage("Trying to execute the command for " + str(n + 1) + " time", verbose)
            try:
                commandReference = self.ntlmClientConnection.run_command(shellStream, command)

                time.sleep(self.sleepTimeForServerResponse)
    
                stdoutFullString, stderrFullString, return_code = self.ntlmClientConnection.get_command_output(shellStream, commandReference)
                stdoutFullString = stdoutFullString.decode("utf-8")
                stderrFullString = stderrFullString.decode("utf-8")

                errorFlag = False
                break
            except Exception:
                unknownException = str(sys.exc_info()[1])
                self.updateStatusMessage("Error occured: "+ unknownException, verbose)
                time.sleep(self.sleepTimeForServerResponse)
                pass

        if errorFlag:
            self.updateStatusMessage("Failed to execute the command on host", verbose)
            return stdoutFullString, stderrFullString, commandReference
        
        self.updateStatusMessage("Command execution has been finished", verbose)
        return stdoutFullString, stderrFullString, commandReference

    def readCommentOutputFromShellStream(self, shellStream, commandReference, verbose = False):
        stdoutFullString = ''
        stderrFullString = ''

        if self.ntlmClientConnection == False:
            self.updateStatusMessage("Invalid ntlm connection. Can't execute command", verbose)
            return stdoutFullString, stderrFullString

        errorFlag = True
        #Try 2 attempts to read the command
        for n in range(0, 2):
            self.updateStatusMessage("Trying to read output for the command for " + str(n + 1) + " time", verbose)
            try:
                time.sleep(self.sleepTimeForServerResponse)

                stdoutFullString, stderrFullString, return_code = self.ntlmClientConnection.get_command_output(shellStream, commandReference)
                stdoutFullString = stdoutFullString.decode("utf-8")
                stderrFullString = stderrFullString.decode("utf-8")

                if stdoutFullString != "" or stderrFullString != "":
                    errorFlag = False
                    break
            
                self.updateStatusMessage("Received empty output.", verbose)
            except Exception:
                unknownException = str(sys.exc_info()[1])
                self.updateStatusMessage("Error occured: "+ unknownException, verbose)
                time.sleep(self.sleepTimeForServerResponse)
                pass
            
        if errorFlag:
            self.updateStatusMessage("Failed to receive the command output from the host", verbose)
            return stdoutFullString, stderrFullString
        
        self.updateStatusMessage("Output received for the command reference", verbose)
        return stdoutFullString, stderrFullString

    def cleanCommandOnShellStream(self, shellStream, commandReference, verbose = False):
        if self.ntlmClientConnection == False:
            self.updateStatusMessage("Invalid ntlm connection. Can't execute command", verbose)
            return True

        try:
            self.ntlmClientConnection.cleanup_command(shellStream, commandReference)

            self.updateStatusMessage("Command cleanup done", verbose)
            return True
        except Exception:
            unknownException = str(sys.exc_info()[1])
            self.updateStatusMessage("Error occured: "+ unknownException, verbose)
            return False

    def closeShellStream(self, shellStream, verbose = False):
        if self.ntlmClientConnection:
            self.ntlmClientConnection.close_shell(shellStream)
            self.updateStatusMessage("Closed shellstream", verbose)
        pass
