Workflow Overall-Flow {
    function ExtractValues($CounterObject){
        $ExtractedObject = @()
        $CounterObject | ForEach-Object -Process {
            $TempObject = [pscustomobject] [ordered] @{ # Only if on PowerShell V3
                Value = $_.CookedValue
                TimeStamp = (Get-Date $_.Timestamp).ToString()
            }
    
            $ExtractedObject += $TempObject
        }
        
        return $ExtractedObject
    }

    $CPUCounter = ""
    $MemoryCounter = ""
    $DiskCounter = ""

    Parallel {
        $workflow:CPUCounter = ExtractValues (Get-Counter -Counter "\Processor(_Total)\% Processor Time" -SampleInterval 1 -MaxSamples 5).CounterSamples
        $workflow:MemoryCounter = ExtractValues (Get-Counter -Counter "\Memory\% Committed Bytes In Use" -SampleInterval 1 -MaxSamples 5).CounterSamples
        $workflow:DiskCounter = ExtractValues (Get-Counter -Counter "\PhysicalDisk(_Total)\% Disk Time" -SampleInterval 1 -MaxSamples 5).CounterSamples
    }

    $ReturnObject = [pscustomobject] [ordered] @{ # Only if on PowerShell V3
        Status = "success"
        CpuCounter = $workflow:CPUCounter
        MemoryCounter = $workflow:MemoryCounter
        DiskCounter = $workflow:DiskCounter
    }

    $ReturnObject | ConvertTo-Json
}

Overall-Flow