#$TopRunningProcessList = Get-Process | Sort-Object CPU -desc | Select-Object Id, ProcessName, CPU -First 25
$TopRunningProcessList = Get-Process | Select-Object Id, ProcessName, CPU

$ReturnObject = [pscustomobject] [ordered] @{ # Only if on PowerShell V3
    Status = "success"
    ShellOutput = $TopRunningProcessList
}

$ReturnObject | ConvertTo-Json