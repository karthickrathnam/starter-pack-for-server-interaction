Stop-Process -Id $IDVAR -Force

$ReturnObject = [pscustomobject] [ordered] @{ # Only if on PowerShell V3
    Status = "success"
    ShellOutput = "success"
}

$ReturnObject | ConvertTo-Json