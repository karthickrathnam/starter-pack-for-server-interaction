#************************************************************************
# Script will fetch the cpu, memory, network and disk usage from the 
# windows machine.
#
# Author: Karthickraja R
# Helpers: Dhanasekaran M
#************************************************************************

Clear-Host
$CpuUsage = (Get-Counter -Counter "\Processor(_Total)\% Processor Time" -SampleInterval 1 -MaxSamples 1).CounterSamples.CookedValue

$MemoryUsage = Get-WmiObject win32_operatingsystem |
Foreach {"{0:N2}" -f ((($_.TotalVisibleMemorySize - $_.FreePhysicalMemory)*100)/ $_.TotalVisibleMemorySize)}

$colInterfaces = Get-WmiObject -class Win32_PerfFormattedData_Tcpip_NetworkInterface | 
Select BytesTotalPersec, CurrentBandwidth,PacketsPersec | where {$_.PacketsPersec -gt 0}
$bitsPerSec = $colInterfaces.BytesTotalPersec * 8
$totalBits = $colInterfaces.CurrentBandwidth
$ErrorActionPreference = "SilentlyContinue" #To hide abnormal Exception like divide by 0
$network_util = (( $bitsPerSec / $totalBits) * 100)
$NetworkUsage = [int]$network_util                    

$DiskUsage = Get-WMIObject -Class Win32_PerfFormattedData_PerfDisk_PhysicalDisk -Filter 'Name = "_Total"'
$DiskUsage = [int]$DiskUsage.PercentDiskTime

$PercentageObject = [pscustomobject] [ordered] @{ # Only if on PowerShell V3
    CpuUsage = $CpuUsage
    MemoryUsage = $MemoryUsage
    DiskUsage = $DiskUsage
    NetworkUsage = $NetworkUsage
}

$ReturnData = [pscustomobject] [ordered] @{ # Only if on PowerShell V3
    Percentage = $PercentageObject
}

$ReturnObject = [pscustomobject] [ordered] @{ # Only if on PowerShell V3
    Status = "success"
    ShellOutput = $ReturnData
}

$ReturnObject | ConvertTo-Json